#include <iostream>
#include <fstream>

int main(int argc, char** argv) {
    if(argc != 2) {
        std::cout << "Bad arguments." << std::endl;
        return 0;
    }
    std::ifstream file(argv[1]);
    if(file.is_open()) {
        char c;
        int floor = 0;
        int count = 0;
        bool tmp = true;
        file.get(c);
        while(!file.eof()) {
            if(c == '(') {
                ++floor;
                ++count;
            } else if(c == ')') {
                --floor;
                ++count;
            }
            file.get(c);
            if(floor < 0 && tmp) {
                tmp = false;
                std::cout << "Task 2: " << count << std::endl;
            }
        }
        std::cout << "Task 1: " << floor << std::endl;
    }
    file.close();
    return 0;
}
