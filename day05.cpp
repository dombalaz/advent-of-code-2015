#include <iostream>
#include <fstream>

using namespace std;

bool containsAtLestThreeVowels(const string& word) {
    int contains = 0;
    const string vowels("aeiou");
    for(unsigned int i = 0; i < word.size(); ++i) {
        for(unsigned int j = 0; j < vowels.size(); ++j) {
            if(word[i] == vowels[j]) {
                ++contains;
                break;
            }
        }
    }
    return contains >= 3;
}

bool containsLetterTwiceInRow(const string& word) {
    for(unsigned int i = 1; i <= word.size(); ++i) {
        if(word[i] == word[i - 1]) {
            return true;
        }
    }
    return false;
}

bool notContainsString(const string& word) {
    if(word.find("ab") == word.npos && word.find("cd") == word.npos && word.find("pq") == word.npos && word.find("xy") == word.npos) {
        return true;
    } else {
        return false;
    }
}

bool containsPairWithoutOverlapping(const string& word) {
    for(unsigned int i = 0; i < word.size() - 3; ++i) {
        string subword = word.substr(i, 2);
        if(word.find(subword, i + 2) != word.npos) {
            return true;
        }
    }
    return false;
}

bool containsRepeatLetter(const string& word) {
    for(unsigned int i = 0; i < word.size() - 2; ++i) {
        if(word[i] == word[i + 2]) {
            return true;
        }
    }
    return false;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cout << "Bad arguments." << endl;
        return 0;
    }
    ifstream file(argv[1]);
    if(file.is_open()) {
        int count1 = 0, count2 = 0;
        string line;
        getline(file, line);
        while(file.good()) {
            if(containsAtLestThreeVowels(line) && containsLetterTwiceInRow(line) && notContainsString(line)) {
                ++count1;
            }
            if(containsPairWithoutOverlapping(line) && containsRepeatLetter(line)) {
                ++count2;
            }
            getline(file, line);
        }
        cout << "Task 1: " << count1 << endl;
        cout << "Task 2: " << count2 << endl;
    }
    file.close();

    return 0;
}
