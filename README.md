# Advent of Code 2015
This project is for solving Advent of Code 2015 problems from the website [Advent of Code](http://adventofcode.com/2015). These are not official solutions.

## Dependencies
- C++ compiler supporting C++11.
- [CMake](https://cmake.org/) at least v2.8.0
- [OpenSSL](https://www.openssl.org/) for problem *Day 04*

## Currently solved problems
- Day 01
- Day 02
- Day 03
- Day 04
- Day 05
- Day 06

## Building from sources
In project directory run
```
$ mkdir build
$ cd build
$ cmake ../
$ make
```

## Build just some days
If you would like to exclude some days from building you can run CMake with these options
- `$ cmake -D BUILD_DAY="<xy>" ../`
- `$ cmake -D DONT_BUILD_DAY="<xy>" ../`

where xy is number of day and days are separated with spaces.

## Run binaries
```
$ ./dayXX <path-to-input-file>
```
