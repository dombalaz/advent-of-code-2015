#include <iostream>
#include <fstream>
#include <cstdio>
#include <climits>
#include <openssl/md5.h>

using namespace std;

int main(int argc, char** argv) {
    if(argc != 2) {
        cout << "Bad arguments." << endl;
        return 0;
    }
    ifstream file(argv[1]);
    if(file.is_open()) {
        string line, r;
        char *tmp = new char(1);
        unsigned char *md5_result = new unsigned char(MD5_DIGEST_LENGTH);
        getline(file, line);
        unsigned int i = 0;
        for(; i < UINT_MAX; ++i) {
            string input = line + to_string(i);
            MD5(reinterpret_cast<const unsigned char *>(input.c_str()), input.length(), md5_result);
            for (unsigned int j = 0; j < MD5_DIGEST_LENGTH; ++j) {
              sprintf(tmp, "%02x",  md5_result[j]);
              r.append(tmp);
            }
            if(r.substr(0, 5) == "00000") {
                break;
            }
            r.clear();
        }
        cout << "Task 1: " << i << endl;
        delete md5_result;
        delete tmp;
    }
    file.close();

    file.open(argv[1]);
    if(file.is_open()) {
        string line, r;
        char *tmp = new char(1);
        unsigned char *md5_result = new unsigned char(MD5_DIGEST_LENGTH);
        getline(file, line);
        unsigned int i = 0;
        for(; i < UINT_MAX; ++i) {
            string input = line + to_string(i);
            MD5(reinterpret_cast<const unsigned char *>(input.c_str()), input.length(), md5_result);
            for (unsigned int j = 0; j < MD5_DIGEST_LENGTH; ++j) {
              sprintf(tmp, "%02x",  md5_result[j]);
              r.append(tmp);
            }
            if(r.substr(0, 6) == "000000") {
                break;
            }
            r.clear();
        }
        cout << "Task 2: " << i << endl;
        delete md5_result;
        delete tmp;
    }
    file.close();

    return 0;
}
