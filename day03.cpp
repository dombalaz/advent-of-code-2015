#include <iostream>
#include <fstream>
#include <list>

using namespace std;

int main(int argc, char** argv) {
    if(argc != 2) {
        cout << "Bad arguments." << endl;
        return 0;
    }
    list<pair<int, int>> coordinates;
    ifstream file(argv[1]);
    if(file.is_open()) {
        int x = 0, y = 0;
        coordinates.push_back(pair<int, int>(x, y));
        char c = file.get();
        while(file.good()) {
            switch (c) {
            case '^':
                ++x;
                break;
            case '>':
                ++y;
                break;
            case 'v':
                --x;
                break;
            case '<':
                --y;
                break;
            default:
                break;
            }
            coordinates.push_back(pair<int, int>(x, y));
            c = file.get();
        }
        coordinates.sort();
        coordinates.unique();
        cout << "Task 1: " << coordinates.size() << endl;
    }

    file.close();
    file.open(argv[1]);

    if(file.is_open()) {
        coordinates.clear();
        int xSanta = 0, ySanta = 0, xRobo = 0, yRobo = 0, i = 0;
        coordinates.push_back(pair<int, int>(0, 0));
        char c = file.get();
        while(file.good()) {
            if(i++ % 2) {
               switch (c) {
               case '^':
                   ++xSanta;
                   break;
               case '>':
                   ++ySanta;
                   break;
               case 'v':
                   --xSanta;
                   break;
               case '<':
                   --ySanta;
                   break;
               default:
                   break;
               }
               coordinates.push_back(pair<int, int>(xSanta, ySanta));
            } else {
                switch (c) {
                case '^':
                    ++xRobo;
                    break;
                case '>':
                    ++yRobo;
                    break;
                case 'v':
                    --xRobo;
                    break;
                case '<':
                    --yRobo;
                    break;
                default:
                    break;
                }
                coordinates.push_back(pair<int, int>(xRobo, yRobo));
            }
            c = file.get();
        }
        coordinates.sort();
        coordinates.unique();
        cout << "Task 2: " << coordinates.size() << endl;
    }
    file.close();
    return 0;
}
