#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    if(argc != 2) {
        cout << "Bad arguments." << endl;
        return 0;
    }
    ifstream file(argv[1]);
     if(file.is_open()) {
        string line, tmp;
        uint64_t a = 0, b = 0, c = 0, result1 = 0, result2 = 0;
        getline(file, line);
        while(file.good()) {
            tmp = line.substr(0, line.find_first_of("x"));
            a = stoi(tmp);
            line = line.substr(line.find_first_of("x") + 1);

            tmp = line.substr(0, line.find_first_of("x"));
            b = stoi(tmp);

            tmp = line.substr(line.find_first_of("x") + 1);
            c = stoi(tmp);

            result1 += 2 * (a * b + a * c + b * c) + std::min(a * b, std::min(b * c, a * c));

            uint64_t lowest = std::min(a, std::min(b, c));
            result2 += lowest + lowest + a * b * c;
            if(lowest == a) {
                result2 += std::min(b, c) + std::min(b, c);
            } else if(lowest == b) {
                result2 += std::min(a, c) + std::min(a, c);
            } else {
                result2 += std::min(a, b) + std::min(a, b);
            }

            getline(file, line);
        }
        cout << "Task 1: " << result1 << endl;
        cout << "Task 2: " << result2 << endl;
    }
    file.close();
    return 0;
}
