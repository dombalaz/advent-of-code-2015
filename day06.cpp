#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

enum class Action {On, Off, Toggle};

int main(int argc, char** argv) {
    if(argc != 2) {
        cout << "Bad arguments." << endl;
        return 0;
    }
    ifstream file(argv[1]);
    if(file.is_open()) {
        vector<vector<bool>> grid;
        grid.resize(1000, std::vector<bool>(1000, false));
        string line;
        getline(file, line);
        while(file.good()) {
            Action action;
            string inst = line.substr(0, line.find_first_of(" "));
            line = line.substr(line.find_first_of(" ") + 1);
            if(inst == "toggle") {
                action = Action::Toggle;
            } else {
                inst = line.substr(0, line.find_first_of(" "));
                line = line.substr(line.find_first_of(" ") + 1);
                if(inst == "on") {
                    action = Action::On;
                } else {
                    action = Action::Off;
                }
            }
            int ax, ay, bx, by;
            string coordinate = line.substr(0, line.find_first_of(","));
            line = line.substr(line.find_first_of(",") + 1);
            ax = stoi(coordinate);

            coordinate = line.substr(0, line.find_first_of(" "));
            line = line.substr(line.find_first_of(" ") + 1);
            ay = stoi(coordinate);

            line = line.substr(line.find_first_of(" "));

            coordinate = line.substr(0, line.find_first_of(","));
            line = line.substr(line.find_first_of(",") + 1);
            bx = stoi(coordinate);

            by = stoi(line);

            switch(action) {
            case Action::On :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        grid[i][j] = true;
                    }
                }
                break;
            case Action::Off :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        grid[i][j] = false;
                    }
                }
                break;
            case Action::Toggle :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        grid[i][j] = !(grid[i][j]);
                    }
                }
                break;
            }

            getline(file, line);
        }
        int count = 0;
        for(const vector<bool>& i : grid) {
            for(const bool& j : i) {
                if(j) {
                    ++count;
                }
            }
        }
        cout << "Task 1: " << count << endl;
    }
    file.close();

    file.open(argv[1]);
    if(file.is_open()) {
        vector<vector<int>> grid;
        grid.resize(1000, std::vector<int>(1000, 0));
        string line;
        getline(file, line);
        while(file.good()) {
            Action action;
            string inst = line.substr(0, line.find_first_of(" "));
            line = line.substr(line.find_first_of(" ") + 1);
            if(inst == "toggle") {
                action = Action::Toggle;
            } else {
                inst = line.substr(0, line.find_first_of(" "));
                line = line.substr(line.find_first_of(" ") + 1);
                if(inst == "on") {
                    action = Action::On;
                } else {
                    action = Action::Off;
                }
            }
            int ax, ay, bx, by;
            string coordinate = line.substr(0, line.find_first_of(","));
            line = line.substr(line.find_first_of(",") + 1);
            ax = stoi(coordinate);

            coordinate = line.substr(0, line.find_first_of(" "));
            line = line.substr(line.find_first_of(" ") + 1);
            ay = stoi(coordinate);

            line = line.substr(line.find_first_of(" "));

            coordinate = line.substr(0, line.find_first_of(","));
            line = line.substr(line.find_first_of(",") + 1);
            bx = stoi(coordinate);

            by = stoi(line);

            switch(action) {
            case Action::On :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        grid[i][j] += 1;
                    }
                }
                break;
            case Action::Off :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        if(grid[i][j] > 0) {
                            grid[i][j] -= 1;
                        }
                    }
                }
                break;
            case Action::Toggle :
                for(int i = ay; i <= by; ++i) {
                    for(int j = ax; j <= bx; ++j) {
                        grid[i][j] += 2;
                    }
                }
                break;
            }

            getline(file, line);
        }
        int count = 0;
        for(const vector<int>& i : grid) {
            for(const int& j : i) {
                count += j;
            }
        }
        cout << "Task 2: " << count << endl;
    }
    file.close();

    return 0;
}
